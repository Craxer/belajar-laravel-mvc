<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="welcome.html", method="POST">
        <p> Sign Up Form </p>
        <li>
            <label for="firstname"> First name : </label><br>
            <input type="text" name="firstname" id="firstname"><br>
        </li> <br>
        <li>
            <label for="lastname"> Last name : </label><br>
            <input type="text" name="lastname" id="lastname"><br>
        </li> <br>
        <li>
            <label for="gender"> Gender : </label><br>
            <input type="radio" name="gender" id="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" name="gender" id="gender" value="female">
            <label for="male">Female</label><br>
            <input type="radio" name="gender" id="gender" value="other">
            <label for="male">Other</label><br>
        </li> <br>
        <li>
            <label for="nationality">Nationality :</label><br>
            <select id="nationality" name="nationality">
              <option value="indonesian">Indonesian</option>
              <option value="malaysian">Malaysian</option>
              <option value="singaporean">Singaporean</option>
              <option value="australian">Australian</option>
            </select> <br><br>
        </li>
        <li>
            <label for="language"> Language Spoken : </label><br>
            <input type="checkbox" name="indonesia" id="indonesia" value="indonesia">
            <label for="indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" name="english" id="english" value="english">
            <label for="indonesia">English</label><br>
            <input type="checkbox" name="other" id="other" value="other">
            <label for="other">Other</label><br>
        </li>
        <label for=""><br> <br> Bio </label> <br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br> <br> <input type="submit", value="submit", formaction="welcome1", method="POST">
        @csrf
        <label for="daftar"></label>
    </form>
</body>
</html>

