<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/register', 'AuthController@register');
Route::get('/welcome1', 'AuthController@welcome');
Route::get('/', 'HomeController@home');
Route::post('/welcome1', 'AuthController@welcome_post');
Route::get('/data-table', 'AuthController@datatable');

// Route Laravel Crud
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
