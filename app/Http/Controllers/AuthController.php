<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('/register');
    }
    public function welcome(){
        return view ('/welcome1');
    }
    public function welcome_post(Request $request){
        $namadepan = $request->firstname;
        $namabelakang = $request->lastname;
        return view ('/welcome1', compact('namadepan', 'namabelakang'));
    }
    //public function untuk form dan welcome1
    public function datatable(){
        return view ('/data-table');
    }
}
